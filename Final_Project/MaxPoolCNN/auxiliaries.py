import numpy as np
import os
import pandas as pd
from matplotlib import pyplot as plt
from scipy.io import wavfile
import torch
from torch.utils.data import Dataset, DataLoader
#from torchvision import transforms
from scipy import signal
import librosa as ft
import scipy.ndimage.interpolation as interp
from matplotlib import colors as mcolors
import csv

def log_specgram(audio, sample_rate, eps=1e-7):
    freqs, times, spec = signal.spectrogram(audio,fs=sample_rate,window='hann',nperseg=int(round(20*sample_rate/1e3)),
                                            noverlap=int(round(10*sample_rate /1e3)),detrend=False)
    return freqs, times, np.log(spec.astype(np.float32) + eps)

def transform(data,rate,feature):
    data = data.astype('float32')
    #Extract features
    if feature=='spec':
        f,t,feature = log_specgram(data,rate)
        feature = interp.zoom(feature,zoom=(0.1,1.))
    elif feature=='mfcc':
        feature = ft.mfcc(data, sr=rate, n_mfcc=100)
    #Normalize
    feature = (feature-np.amin(feature))/(np.amax(feature)-np.amin(feature))
    #ToTensor
    return torch.from_numpy(feature)


class sounds(Dataset):
    def __init__(self, N=6631, mode='train', feature ='spec'):
        self.N=N
        self.mode = mode
        self.feature = feature
        self.csv_path = os.path.join(os.path.dirname(os.getcwd()),'Split_Files', self.mode+'_segment.csv')
        self.csv = pd.read_csv(self.csv_path)
        self.classes = {name:i for i, name in enumerate(self.csv['label'].unique())}
        self.inv_classes = {self.classes[n]:n for n in self.classes.keys()}

    def __len__(self):
        if self.N=='all':
            return len(self.csv)
        else:
            return self.N

    def __getitem__(self, idx):
        fnum = self.csv['fname'][idx]
        fname = os.path.join(os.path.dirname(os.getcwd()),'audio_train',fnum)
        #fname = '/media/karsten_dl/QS/Dropbox/Data_Dump/AudioChallenge/audio_train/'+fnum

        rate, data = wavfile.read(fname)
        features   = transform(data,rate,self.feature)

        if self.mode=="train" or self.mode=="val":
            label = self.classes[self.csv["label"][idx]]
            return {'x':features, 'y':label, 'data':data, 'rate':rate}

        elif self.mode=="test":
            return {'x':features}

def collate_variable_sizes(batch):
    data   = [item['x'] for item in batch]
    target = torch.LongTensor([item['y'] for item in batch])
    return {'x':data, 'y':target}


def write_log(logname, epoch, train_loss, val_loss, train_acc, val_acc):
    with open(logname,"a") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        if epoch==0:
            writer.writerow(["Epoch", "Training Loss", "Validation Loss", "Training Accuracy", "Validation Accuracy"])
        writer.writerow([epoch, train_loss, val_loss, train_acc, val_acc])

def plot_learning_curves(ax,name):
    colors = dict(**mcolors.CSS4_COLORS)
    keys = ['firebrick', 'darkorange', 'darkgreen', 'navy', 'black', 'darkmagenta', 'darkolivegreen', 'hotpink', 'gold']

    data = pd.read_csv(name,header=0)
    num_lines = len(ax[0].lines)//2
    ax[0].plot(data["Epoch"], data["Training Loss"],color=colors[keys[num_lines]], label=os.path.split(os.path.split(name)[0])[1]+' | Train')
    ax[0].plot(data["Epoch"], data["Validation Loss"],color=colors[keys[num_lines]],linestyle='--',label=os.path.split(os.path.split(name)[0])[1]+' | Validation')
    ax[1].plot(data["Epoch"], data["Training Accuracy"],color=colors[keys[num_lines]],label=os.path.split(os.path.split(name)[0])[1]+' | Train')
    ax[1].plot(data["Epoch"], data["Validation Accuracy"],color=colors[keys[num_lines]],linestyle='--',label=os.path.split(os.path.split(name)[0])[1]+' | Validation')

def make_learning_curves_fig(name):
    fig = plt.figure(figsize=(15,5))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax = (ax1,ax2)

    plot_learning_curves(ax,name)

    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('Loss')
    ax1.legend(loc='upper right', fontsize=8)

    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('Accuracy')
    ax2.legend(loc='lower right', fontsize=8)
    plt.tight_layout()

    plt.savefig(os.path.join(os.path.split(name)[0],'Loss_Log.pdf'))
