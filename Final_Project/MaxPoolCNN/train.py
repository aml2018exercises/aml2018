'''-----------------------------------------------------------------------------
--------------------------------------------------------------------------------
----------------------   CNN for sound classification    ----------------------
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

Written by: Elias Eulig for the Advanced Machine Learning 2018 lecture
'''

'''-----------------------------------------------------------------------------
-----------------------------   Load modules    --------------------------------
-----------------------------------------------------------------------------'''
import argparse
import numpy as np
import pandas as pd
import os
import matplotlib
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from matplotlib.backends.backend_pdf import PdfPages
from random import randint
import time
import datetime
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import RandomSampler
from torch.optim.lr_scheduler import StepLR
from scipy import ndimage
import torch.nn.functional as F
import auxiliaries as aux
import json
import ast
import csv
import gc
import re
import sys
import pickle
import itertools as it
import IPython

'''-----------------------------------------------------------------------------
------------------------------  Initial setup    -------------------------------
-----------------------------------------------------------------------------'''
parser = argparse.ArgumentParser()

parser.add_argument('--savepath', default = os.path.join(os.getcwd(),"results"), help='Where to store the results')
parser.add_argument('--nepochs', type=int, default=25, help='number of epochs to train for, default = 500')
parser.add_argument('--mbs', type=int, default=64, help='mini-batch size, default=10')
parser.add_argument('--cuda', action='store_true', help='enables cuda')
parser.add_argument('--n_workers', type=int, default=8, help='number of epochs to train for, default = 500')
parser.add_argument('--lr', type=float, default=0.001, help='learning rate, default=0.0001')
parser.add_argument('--b1', type=float, default=0.9, help='learning rate, default=0.0001')
parser.add_argument('--b2', type=float, default=0.999, help='learning rate, default=0.0001')
parser.add_argument('--pool_mode', default = 'max', help='Pooling mode to use')
parser.add_argument('--model_type', default = 'simple', help='Type of model to use')
opt = parser.parse_args([])


rundate     = datetime.datetime.now()
savetime    = "{:02d}_{:02d}_{:02d}__{:02d}{:02d}{:02d}".format(rundate.day, rundate.month, rundate.year, rundate.hour, rundate.minute, rundate.second)
opt.savepath = os.path.join(opt.savepath,"MaxPoolCNN_"+savetime)

if not os.path.exists(opt.savepath):
    os.makedirs(opt.savepath)

with open(os.path.join(opt.savepath,'Hyperparameters.txt'),'w') as f:
    json.dump(vars(opt), f)

'''-----------------------------------------------------------------------------
-------------------------------  Data setup    ---------------------------------
-----------------------------------------------------------------------------'''
# Train_Data = aux.sounds(N=6631,mode='train',feature ='spec')
Train_Data = aux.sounds(N='all',mode='train',feature ='spec')
Dataloader_Train = DataLoader(Train_Data, batch_size=opt.mbs, shuffle=True, num_workers=opt.n_workers, collate_fn=aux.collate_variable_sizes)

Val_Data = aux.sounds(N='all',mode='val',feature ='spec')
# Val_Data = aux.sounds(N=1894,mode='val',feature ='spec')
Dataloader_Val = DataLoader(Val_Data, batch_size=opt.mbs, shuffle=True, num_workers=opt.n_workers, collate_fn=aux.collate_variable_sizes)

'''-----------------------------------------------------------------------------
-------------------------  Network and Loss setup    ---------------------------
-----------------------------------------------------------------------------'''
import model
if opt.model_type == 'simple':
    net = model.max_pool_cnn_closer_to_orig(n_features=44,n_classes=41,filters=[3,5,9,29],P=32,pool_mode=opt.pool_mode)
elif opt.model_type == 'doubleconv':
    net = model.max_pool_cnn_closer_to_orig_more_convs(n_features=44,n_classes=41,filters=[3,5,9],P=32,pool_mode=opt.pool_mode)

if opt.cuda:
    net.cuda()

criterion=nn.NLLLoss()
optimizer = optim.Adam(net.parameters(), lr=opt.lr, betas=(opt.b1,opt.b2))
'''-----------------------------------------------------------------------------
-----------------------------  Train Network    --------------------------------
-----------------------------------------------------------------------------'''

from tqdm import tqdm

for epoch in tqdm(range(opt.nepochs), position=0, desc='Epoch'):
    train_loss = 0.0
    train_acc = 0.0
    val_loss = 0.0
    val_acc = 0.0

    ''' Train Network'''
    net.train()
    for i_batch, sample_batched in enumerate(tqdm(Dataloader_Train, position=1, desc='TRAIN Iteration')):
        inputs, target = sample_batched['x'], Variable(sample_batched['y'])
        inputs = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]

        if opt.cuda:
            inputs = [x.cuda() for x in inputs]
            target = target.cuda()


        output = net(inputs)
        pred   = output.data.max(1)[1]

        train_acc += np.sum(pred.cpu().numpy()==target.data.cpu().numpy())
        loss = criterion(output, target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        train_loss += loss.cpu().data.numpy()[0]
        # print('TRAIN: epoch %2d of %2d | mini-batch %3d of %3d | loss: %.4f' % (epoch + 1, opt.nepochs, i_batch + 1, len(Dataloader_Train), loss.data.item()))

    ''' Validate Network'''
    net.eval()
    for i_batch, sample_batched in enumerate(tqdm(Dataloader_Val, position=1, desc='VAL Iteration')):
        inputs, target = sample_batched['x'], Variable(sample_batched['y'])
        inputs = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]

        if opt.cuda:
            inputs = [x.cuda() for x in inputs]
            target = target.cuda()


        output = net(inputs)
        pred = output.data.max(1)[1]
        val_acc += np.sum(pred.cpu().numpy()==target.data.cpu().numpy())
        loss = criterion(output, target)

        val_loss += loss.cpu().data.numpy()[0]

    val_loss /=len(Dataloader_Val)
    val_acc /= (len(Dataloader_Val)*opt.mbs)
    # print('VALIDATE: epoch %2d of %2d | loss: %.4f | acc: %.4f' % (epoch + 1, opt.nepochs, val_loss, val_acc))

    train_loss /=len(Dataloader_Train)
    train_acc /= (len(Dataloader_Train)*opt.mbs)


    aux.write_log(opt.savepath+"/Loss_Log.csv", epoch, train_loss, val_loss, train_acc, val_acc)

aux.make_learning_curves_fig(os.path.join(opt.savepath,'Loss_Log.csv'))
'''
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import torch
from torch.autograd import Variable

n_features=44
n_classes=41
filters=[3,5]
P=32
activation=nn.ReLU()
convs1 = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(22,f),padding=(0,0), stride=(2,2)) for f in filters])
convs2 = nn.ModuleList([nn.Conv2d(P, P, kernel_size=(12,f),padding=(0,0), stride=(2,2)) for f in filters])

linear = nn.Linear(int(P*len(filters)),n_classes)
softmax = nn.LogSoftmax(dim=0)
pool_mode='max'


sample_batched=next(iter(Dataloader_Train))
inputs, target = sample_batched['x'], Variable(sample_batched['y'])
x = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]

inputs = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]

val_acc = 0.0
output = net(inputs)
pred = output.data.max(1)[1]
val_acc += torch.sum(pred==target.data)

val_acc
val_acc+=8
val_acc/=64.
val_acc
inval=x[0]

features1 = [activation(convs1[i](inval)) for i in range(len(convs1))]
features2 = [activation(convs2[i](features1[i])) for i in range(len(convs2))]




classification = []
for inval in x:
    features = torch.cat([self.activation(self.convs[i](inval)) for i in range(len(self.convs))],1)
    if self.pool_mode=='mean':
        pooled = torch.mean(features,dim=3)
    elif self.pool_mode=='max':
        pooled,_=torch.max(features,dim=3)
    pooled_all.append(torch.squeeze(pooled))


class max_pool_cnn(nn.Module):
    def __init__(self,n_features=66,n_classes=41,filters=[3,5,9,19,25],P=16, pool_mode='max'):
        super(max_pool_cnn, self).__init__()
        self.pool_mode = pool_mode
        self.activation = nn.ReLU()
        self.convs = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(n_features,f),padding=(0,int((f-1)/2))) for f in filters])
        self.linear = nn.Linear(int(P*len(filters)),n_classes)
        self.softmax = nn.LogSoftmax(dim=1)
    def forward(self, x):

        pooled_all = []
        for inval in x:
            features = torch.cat([self.activation(self.convs[i](inval)) for i in range(len(self.convs))],1)
            if self.pool_mode=='mean':
                pooled = torch.mean(features,dim=3)
            elif self.pool_mode=='max':
                pooled,_=torch.max(features,dim=3)
            pooled_all.append(torch.squeeze(pooled))

        return self.softmax(self.linear(torch.stack(pooled_all,0)))



plt.plot(grid_search_losses[list(grid_search_losses.keys())[1]])
plt.figure(figsize=(10,50))
for idx,k in enumerate(grid_search_losses.keys()):
    plt.subplot(len(list(grid_search_losses.keys())),1,idx+1)
    plt.plot(grid_search_losses[k])
batch['x'][2]
IPython.display.Audio(batch['x'][5])
Train_Data.inv_classes[batch['y'][6].item()]

plt.imshow(batch['x'][6])
plt.title(Train_Data.inv_classes[batch['y'][6].item()])


inp=batch['x']
y = batch['y']
loss = nn.NLLLoss()

from model import cnn


net = cnn(n_features=44,n_classes=41,filters=[3,5,9],P=16)
out = net(inp)

inp

out.shape
y.shape

loss(out,y)

filters = [3,5]
P=2
n_features=44
n_classes=41
mods = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(n_features,f),padding=(0,(f-1)/2)) for f in filters])
relu = nn.ReLU()
linear = nn.Linear(int(P*len(filters)),n_classes)
softmax = nn.Softmax(dim=1)

pooled_all = []
for b in range(len(inp)):
    features = torch.cat([relu(mods[i](torch.unsqueeze(torch.unsqueeze(inp[b],0),0))) for i in range(len(mods))],1)
    pooled_all.append(torch.squeeze(torch.cat([F.max_pool1d(features[:,i,:,:], kernel_size=features.size()[3]) for i in range(features.size()[1])],2)))
max_features = torch.stack(pooled_all,0)


torch.sum(softmax(linear(max_features))[2,:])
torch.unsqueeze(features[:,0,:,:],1).shape
maxpool(features)
pooled = [F.max_pool1d(features[i][], kernel_size=f.size()[2:]) for i in range(len(features)) for p in range(P)  ]

len(features)

def plot_wav_features(idx):
    sample = Train_Data[idx]
    time=np.linspace(0, len(sample['data'])/sample['rate'], num=len(sample['data']))
    plt.suptitle(Train_Data.inv_classes[sample['y']])
    plt.subplot(2,1,1)
    plt.imshow(sample['x'],aspect="auto")
    plt.xticks([])
    plt.ylabel('Feature')
    plt.grid(False)

    plt.subplot(2,1,2)
    plt.plot(time,sample['data'])
    plt.xlim([0,time[-1]])
    plt.xlabel('Time [s]')
    plt.ylabel('Amplitude')
    plt.tight_layout(pad=0.1)
    plt.subplots_adjust(top=0.92)


def plot_wav_features(fig, pos, idx):
    sample = Train_Data[idx]
    time=np.linspace(0, len(sample['data'])/sample['rate'], num=len(sample['data']))
    plt.suptitle(Train_Data.inv_classes[sample['y']])
    plt.Subplot(fig, pos[0])
    plt.imshow(sample['x'],aspect="auto")
    plt.xticks([])
    plt.ylabel('Feature')
    plt.grid(False)

    plt.Subplot(fig, pos[1])
    plt.plot(time,sample['data'])
    plt.xlim([0,time[-1]])
    plt.xlabel('Time [s]')
    plt.ylabel('Amplitude')
    #plt.tight_layout(pad=0.1)
    #plt.subplots_adjust(top=0.92)

import matplotlib.gridspec as gridspec
from matplotlib.ticker import PercentFormatter

fig = plt.figure(figsize=(15, 12))
outer = gridspec.GridSpec(3, 3, wspace=0.3, hspace=0.3)

for i in range(9):
    inner = gridspec.GridSpecFromSubplotSpec(2, 1,
                    subplot_spec=outer[i], wspace=0.1, hspace=0.05)

    sample = Val_Data[i]
    time=np.linspace(0, len(sample['data'])/sample['rate'], num=len(sample['data']))
    #plt.suptitle(Train_Data.inv_classes[sample['y']])
    ax = plt.Subplot(fig, inner[0])
    ax.set_title(Train_Data.inv_classes[sample['y']],fontdict={'fontsize':9})
    ax.imshow(sample['x'],aspect="auto")
    ax.set_xticks([])
    ax.set_ylabel('Feature',size=9)
    ax.grid(False)
    fig.add_subplot(ax)

    ax = plt.Subplot(fig, inner[1])
    ax.plot(time,sample['data'])
    max = np.max(np.abs(sample['data']))
    formatter = PercentFormatter(xmax = max)
    ax.set_xlim([0,time[-1]])
    ax.set_xlabel('Time [s]',size=9)
    ax.yaxis.set_major_formatter(formatter)
    ax.set_ylabel('Amplitude',size=9)
    fig.add_subplot(ax)
fig.show()


#plt.savefig('TrainExamples.pdf')
'''
