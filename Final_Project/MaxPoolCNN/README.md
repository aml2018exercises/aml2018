## Usage Documentation to the implementation of the Max-Pool audiotagging network

* `auxuliaries.py`contains all auxiliary code necessary in order to train the network, namely the dataloader and the function to extract the log-spectrogram from the audio data.
* `model.py` contains different implementations of the network.
* `train.py` contains the training pipeline.