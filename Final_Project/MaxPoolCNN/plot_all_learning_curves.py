import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colors as mcolors
import csv
import os
def plot_learning_curves(ax,name):
    colors = dict(**mcolors.CSS4_COLORS)
    keys = ['firebrick', 'darkorange', 'darkgreen', 'navy', 'black', 'darkmagenta', 'darkolivegreen', 'hotpink', 'gold']

    data = pd.read_csv(name,header=0)
    num_lines = len(ax[0].lines)//2
    ax[0].plot(data["Epoch"], data["Training Loss"],color=colors[keys[num_lines]], label=os.path.split(os.path.split(name)[0])[1]+' | Train')
    ax[0].plot(data["Epoch"], data["Validation Loss"],color=colors[keys[num_lines]],linestyle='--',label=os.path.split(os.path.split(name)[0])[1]+' | Validation')
    ax[1].plot(data["Epoch"], data["Training Accuracy"],color=colors[keys[num_lines]],label=os.path.split(os.path.split(name)[0])[1]+' | Train')
    ax[1].plot(data["Epoch"], data["Validation Accuracy"],color=colors[keys[num_lines]],linestyle='--',label=os.path.split(os.path.split(name)[0])[1]+' | Validation')

def make_learning_curves_fig(name):
    fig = plt.figure(figsize=(15,5))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax = (ax1,ax2)

    plot_learning_curves(ax,name)

    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('Loss')
    ax1.legend(loc='upper right', fontsize=8)

    ax2.set_xlabel('Epochs')
    ax2.set_ylabel('Accuracy')
    ax2.legend(loc='lower right', fontsize=8)
    plt.tight_layout()

results = ['MaxPoolCNN_05_10_2018__144128',
'MaxPoolCNN_05_10_2018__145400',
'MaxPoolCNN_05_10_2018__150608',
'MaxPoolCNN_05_10_2018__151841',
'MaxPoolCNN_05_10_2018__153112',
'MaxPoolCNN_05_10_2018__154320',
'MaxPoolCNN_05_10_2018__155554']


os.path.join(os.getcwd(),results[0],'Loss_Log.csv')
make_learning_curves_fig(os.path.join(os.getcwd(),'results',results[2],'Loss_Log.csv'))


os.path.join(os.getcwd(),'results',results[2],'Loss_Log.csv')
