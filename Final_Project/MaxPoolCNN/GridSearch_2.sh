python train.py --cuda --lr 0.0002  --b1 0.9000 --pool_mode max --model_type doubleconv
python train.py --cuda --lr 0.0010  --b1 0.4500 --pool_mode max --model_type simple
python train.py --cuda --lr 0.0010  --b1 0.4500 --pool_mode max --model_type doubleconv
python train.py --cuda --lr 0.0010  --b1 0.4500 --pool_mode mean --model_type doubleconv
python train.py --cuda --lr 0.0010  --b1 0.9000 --pool_mode max --model_type simple
python train.py --cuda --lr 0.0010  --b1 0.9000 --pool_mode max --model_type doubleconv
python train.py --cuda --lr 0.0010  --b1 0.9000 --pool_mode mean --model_type doubleconv
