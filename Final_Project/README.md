## Introduction to this Audio Tagging Repository

This repo contains three implementations:

* Audio Classification with _Adaptive Pooling CNN_ 
* Audio Classification with _1-Maxpool CNN_ 
* Audio-Sequence Classification with _recurrent network pipelines_ 

The Data used to train and evaluate these implementations is taken from [Kaggle FreeSound Classification](https://www.kaggle.com/c/freesound-audio-tagging/data). Due to the early competition deadline, the test data used is generated from the 
total training data via `Base_Functions/generate_train_val_test_split.py`.

Each Implementation folder has a more detailed documentation about the specific usage.
