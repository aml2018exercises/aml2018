training = []
d_lr = [0.0001,0.0002, 0.0004,0.0005,0.001]
beta1=[0.1,0.2,0.5,0.9]
pool_mode=['max','mean']
model_type= ['simple', 'doubleconv']
for dlr in d_lr:
    for b1 in beta1:
        for pm in pool_mode:
            for mt in model_type:
                training.append('python training.py --cuda --lr %.4f --b1 %.4f --pool_mode %s --model_type %s'  % (dlr,b1,pm,mt))

outF = open("GridSearch_1.sh", "w")
for line in training:
    outF.write(line)
    outF.write("\n")
outF.close()
