from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import torch
from torch.autograd import Variable

class cnn(nn.Module):
    def __init__(self,n_features=44,n_classes=41,filters=[3,5,9],P=16):
        super(cnn, self).__init__()
        self.activation = nn.ReLU()
        self.convs = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(n_features,f),padding=(0,int((f-1)/2))) for f in filters])
        self.linear = nn.Linear(int(P*len(filters)),n_classes)
        self.softmax = nn.LogSoftmax(dim=1)
    def forward(self, x):

        pooled_all = []
        for inval in x:
            features = torch.cat([self.activation(self.convs[i](inval)) for i in range(len(self.convs))],1)
            pooled_all.append(torch.squeeze(torch.cat([F.max_pool1d(features[:,i,:,:], kernel_size=features.size()[3]) for i in range(features.size()[1])],2)))
        max_features = torch.stack(pooled_all,0)

        return self.softmax(self.linear(max_features))

class max_pool_cnn(nn.Module):
    def __init__(self,n_features=66,n_classes=41,filters=[3,5,9,19,25],P=16, pool_mode='max'):
        super(max_pool_cnn, self).__init__()
        self.pool_mode = pool_mode
        self.activation = nn.ReLU()
        self.convs = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(n_features,f),padding=(0,int((f-1)/2))) for f in filters])
        self.linear = nn.Linear(int(P*len(filters)),n_classes)
        self.softmax = nn.LogSoftmax(dim=1)
    def forward(self, x):

        pooled_all = []
        for inval in x:
            features = torch.cat([self.activation(self.convs[i](inval)) for i in range(len(self.convs))],1)
            if self.pool_mode=='mean':
                pooled = torch.mean(features,dim=3)
            elif self.pool_mode=='max':
                pooled,_=torch.max(features,dim=3)
            pooled_all.append(torch.squeeze(pooled))

        return self.softmax(self.linear(torch.stack(pooled_all,0)))


class max_pool_cnn_closer_to_orig(nn.Module):
    def __init__(self,n_features=66,n_classes=41,filters=[3,5,9,19,25],P=32, pool_mode='max'):
        super(max_pool_cnn_closer_to_orig, self).__init__()
        self.pool_mode = pool_mode
        self.activation = nn.ReLU()
        self.convs = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(n_features,f),padding=(0,0), stride=(1,2)) for f in filters])
        self.linear = nn.Linear(int(P*len(filters)),n_classes)
        self.softmax = nn.LogSoftmax(dim=0)
    def forward(self, x):

        pooled_all = []
        classification_all_batches=[]
        for inval in x:
            pooled_all_features=[]
            features = [self.activation(self.convs[i](inval)) for i in range(len(self.convs))]
            for i in range(len(self.convs)):
                if self.pool_mode=='mean':
                    pooled = torch.mean(features[i],dim=3)
                elif self.pool_mode=='max':
                    pooled,_=torch.max(features[i],dim=3)
                pooled_all_features.append(torch.squeeze(pooled))
            classification_all_batches.append(self.softmax(self.linear(torch.cat(pooled_all_features,0))))
        return torch.stack(classification_all_batches,0)

class max_pool_cnn_closer_to_orig_more_convs(nn.Module):
    def __init__(self,n_features=44,n_classes=41,filters=[3,5,9],P=32, pool_mode='max'):
        super(max_pool_cnn_closer_to_orig_more_convs, self).__init__()
        self.pool_mode = pool_mode
        self.activation = nn.ReLU()
        self.convs1 = nn.ModuleList([nn.Conv2d(1, P, kernel_size=(22,f),padding=(0,0), stride=(2,2)) for f in filters])
        self.convs2 = nn.ModuleList([nn.Conv2d(P, P, kernel_size=(12,f),padding=(0,0), stride=(2,2)) for f in filters])
        self.linear = nn.Linear(int(P*len(filters)),n_classes)
        self.softmax = nn.LogSoftmax(dim=0)
    def forward(self, x):

        pooled_all = []
        classification_all_batches=[]
        for inval in x:
            #print(inval.shape)
            pooled_all_features=[]
            features = [self.activation(self.convs1[i](inval)) for i in range(len(self.convs1))]
            features = [self.activation(self.convs2[i](features[i])) for i in range(len(self.convs2))]

            for i in range(len(self.convs2)):
                if self.pool_mode=='mean':
                    pooled = torch.mean(features[i],dim=3)
                elif self.pool_mode=='max':
                    pooled,_=torch.max(features[i],dim=3)
                pooled_all_features.append(torch.squeeze(pooled))
            classification_all_batches.append(self.softmax(self.linear(torch.cat(pooled_all_features,0))))
        return torch.stack(classification_all_batches,0)
