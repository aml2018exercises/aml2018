# Audio File Classification With Conv-based LSTM RNNs
# @author: Benedict Kerres - Heidelberg University

"""======================================="""
"""========= Load Basic Libraries ========"""
"""======================================="""

import argparse
import numpy as np
import pandas as pd
import os
import matplotlib
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from matplotlib.backends.backend_pdf import PdfPages
from random import randint
import time
import datetime
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import RandomSampler
from torch.optim.lr_scheduler import StepLR
from scipy import ndimage
import torch.nn.functional as F
import auxiliaries_eulig as aux
import json
import ast
import csv
import gc
import re
import sys
import pickle
import itertools as it
from IPython import embed

'''-----------------------------------------------------------------------------
------------------------------  Initial setup    -------------------------------
-----------------------------------------------------------------------------'''
parser = argparse.ArgumentParser()

parser.add_argument('--savepath', default = os.path.join(os.getcwd(),"results"), help='Where to store the results')
parser.add_argument('--nepochs', type=int, default=60, help='number of epochs to train for, default = 500')
parser.add_argument('--mbs', type=int, default=1, help='mini-batch size, default=64')
parser.add_argument('--cuda',default= True, action='store_true', help='enables cuda')
parser.add_argument('--n_workers', type=int, default=8, help='number of epochs to train for, default = 8')
parser.add_argument('--lr', type=float, default=0.001, help='learning rate, default=0.0001')
parser.add_argument('--b1', type=float, default=0.9, help='learning rate, default=0.0001')
parser.add_argument('--b2', type=float, default=0.999, help='learning rate, default=0.0001')
parser.add_argument('--pool_mode', default = 'max', help='Pooling mode to use')
parser.add_argument('--model_type', default = 'simple', help='Type of model to use')
opt = parser.parse_args()


rundate     = datetime.datetime.now()
savetime    = "{:02d}_{:02d}_{:02d}__{:02d}{:02d}{:02d}".format(rundate.day, rundate.month, rundate.year, rundate.hour, rundate.minute, rundate.second)
opt.savepath = os.path.join(opt.savepath,"Baseline_CNN"+savetime)


if not os.path.exists(opt.savepath):
    os.makedirs(opt.savepath)

with open(os.path.join(opt.savepath,'Hyperparameters.txt'),'w') as f:
    json.dump(vars(opt), f)


    '''-----------------------------------------------------------------------------
-------------------------------  Data setup    ---------------------------------
-----------------------------------------------------------------------------'''
# Train_Data = aux.sounds(N=6631,mode='train',feature ='spec')
# Train_Data = aux.sounds(N=6631,mode='train',feature ='mfcc')
Train_Data = aux.sounds(N='all',mode='train',feature ='mfcc')
Dataloader_Train = DataLoader(Train_Data, batch_size=opt.mbs, shuffle=True, num_workers=opt.n_workers, collate_fn=aux.collate_variable_sizes)

Val_Data = aux.sounds(N='all',mode='val',feature ='spec')
# Val_Data = aux.sounds(N=1894,mode='val',feature ='spec')
Dataloader_Val = DataLoader(Val_Data, batch_size=opt.mbs, shuffle=True, num_workers=opt.n_workers, collate_fn=aux.collate_variable_sizes)


'''-----------------------------------------------------------------------------
-------------------------  Network and Loss setup    ---------------------------
-----------------------------------------------------------------------------'''
import model
if opt.model_type == 'batchNorm':
    net = model.CNN(batchNorm=True)
else :
	net = model.CNN(batchNorm=False)
net.cuda()

criterion=nn.NLLLoss()
optimizer = optim.Adam(net.parameters(), lr=opt.lr, betas=(opt.b1,opt.b2))
'''-----------------------------------------------------------------------------
-----------------------------  Train Network    --------------------------------
-----------------------------------------------------------------------------'''


from tqdm import tqdm

for epoch in tqdm(range(opt.nepochs), position=0, desc='Epoch'):
    train_loss = 0.0
    train_acc = 0.0
    val_loss = 0.0
    val_acc = 0.0

    ''' Train Network'''
    net.train()
    for i_batch, sample_batched in enumerate(tqdm(Dataloader_Train, position=1, desc='TRAIN Iteration')):
        inputs, target = sample_batched['x'], Variable(sample_batched['y'])
       
        inputs = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]
        
        
        inputs = inputs[0].cuda()
        inputs = inputs.float()
        #target = torch.LongTensor(target.float())
        target = target.type(torch.cuda.LongTensor)
        #target = target.cuda()

        output = net(inputs)
        pred   = output.data.max(1)[1]

        train_acc += np.sum(pred.cpu().numpy()==target.data.cpu().numpy())
        loss = criterion(output, target)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        #train_loss += loss.cpu().data.numpy()[0]
        # print('TRAIN: epoch %2d of %2d | mini-batch %3d of %3d | loss: %.4f' % (epoch + 1, opt.nepochs, i_batch + 1, len(Dataloader_Train), loss.data.item()))

    ''' Validate Network'''
    net.eval()
    for i_batch, sample_batched in enumerate(tqdm(Dataloader_Val, position=1, desc='VAL Iteration')):
        inputs, target = sample_batched['x'], Variable(sample_batched['y'])
        inputs = [Variable(torch.unsqueeze(torch.unsqueeze(x,0),0)) for x in inputs]

        inputs = inputs[0].cuda()
        inputs = inputs.float()
        target = target.type(torch.cuda.LongTensor)


        output = net(inputs)
        pred = output.data.max(1)[1]
        val_acc += np.sum(pred.cpu().numpy()==target.data.cpu().numpy())
        loss = criterion(output, target)

        #val_loss += loss.cpu().data.numpy()[0]

    val_loss /=len(Dataloader_Val)
    val_acc /= (len(Dataloader_Val)*opt.mbs)
    # print('VALIDATE: epoch %2d of %2d | loss: %.4f | acc: %.4f' % (epoch + 1, opt.nepochs, val_loss, val_acc))

    train_loss /=len(Dataloader_Train)
    train_acc /= (len(Dataloader_Train)*opt.mbs)


    aux.write_log(opt.savepath+"/Loss_Log.csv", epoch, train_loss, val_loss, train_acc, val_acc)

aux.make_learning_curves_fig(os.path.join(opt.savepath,'Loss_Log.csv'))