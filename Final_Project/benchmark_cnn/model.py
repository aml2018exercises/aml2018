# Audio File Classification With Conv-based LSTM RNNs
# @author: Benedict Kerres - Heidelberg University

"""======================================="""
"""========= Load Basic Libraries ========"""
"""======================================="""
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import torch
from torch.autograd import Variable



def conv(batchNorm, in_planes, out_planes, kernel_size=3, stride=1):
    
    if batchNorm:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=False),
            nn.BatchNorm2d(out_planes),
            nn.LeakyReLU(0.1,inplace=True)
        )
    else:
        return nn.Sequential(
            nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=(kernel_size-1)//2, bias=True),
            nn.LeakyReLU(0.1,inplace=True)
        )






class CNN2(nn.Module):
    def __init__(self, batchNorm=True):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.pool  = nn.MaxPool2d(2,2)
        self.conv2 = nn.Conv2d(6, 16, 3)
        self.max_pool2d = torch.nn.AdaptiveMaxPool2d((30,30))
        self.fc1   = nn.Linear(30*30, 120)
        self.fc2   = nn.Linear(120, 84)
        self.fc3   = nn.Linear(84, 44)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = self.max_pool2d(x)
        x = x.view(-1, 30*30)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.log_softmax(x)




class CNN(nn.Module):
	def __init__(self, batchNorm=True):
		super(CNN, self).__init__()
		self.batchNorm = batchNorm
		self.conv1 = conv(self.batchNorm, 1,32, kernel_size = 3)
		self.conv2 = conv(self.batchNorm, 32,64, kernel_size = 3)
		self.conv3 = conv(self.batchNorm, 64,128, kernel_size = 3)
		self.conv4 = conv(self.batchNorm, 128,1, kernel_size = 3)
		self.shrink = torch.nn.AdaptiveMaxPool2d((10,10))
		self.classifier = nn.Linear(100,44)

	def forward(self,x):
		out_conv2 = self.conv2(self.conv1(x))
		out_conv3 = self.conv3(out_conv2)
		out_conv4 = self.conv4(out_conv3)
		out_shrink = self.shrink(out_conv4)
		out_shrink2 = out_shrink.view(-1,100)
		out = self.classifier(out_shrink2)


		return F.log_softmax(out, dim=1)


