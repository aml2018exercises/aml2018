# Audio File Classification With Conv-based LSTM RNNs
# @author: Karsten Roth - Heidelberg University

"""======================================="""
"""========= Load Basic Libraries ========"""
"""======================================="""
import os,json,sys,gc,time,datetime,imp,argparse
import torch, torch.nn as nn, torch.optim as to, numpy as np
import matplotlib.pyplot as plt
import pickle as pkl

from torch.optim import lr_scheduler
from torch.autograd import Variable
from torch.utils.data import DataLoader

from tqdm import tqdm, trange

os.chdir('/media/karsten_dl/QS/Dropbox/University/Files/Master/SS18/Advanced_Machine_Learning/aml2018/Final_Project/RNN_and_CNN')
# os.chdir("/media/karsten_dl/QS/Dropbox/Projects/Current_projects/lesionsegmentation_pytorch/Main_Scripts/Standard_Segmentation/Slice_Style")
# sys.path.insert(0, os.getcwd()+'/../../../Helper_Functions')
# sys.path.insert(0, os.getcwd()+'/../../../Network_Library')

import auxiliaries as aux
import Network_Library as netlib
# from IPython import embed

# ###NOTE: This line is necessary to avoid the "too many open files"-Error!
# if int(torch.__version__.split(".")[1])>2:
#     torch.multiprocessing.set_sharing_strategy('file_system')
# torch.multiprocessing.set_start_method('spawn')


"""======================================="""
"""========= Input Arguments ============="""
"""======================================="""
parse_in = argparse.ArgumentParser()
#Training arguments
parse_in.add_argument("--n_epochs",         type=int, default=100,  help="Number of training epochs.")
parse_in.add_argument("--perc_data",        type=float, default=1., help="Percentage of training data to use.")
parse_in.add_argument("--kernels",          type=int,   default=7,  help="Number of kernels to use for data loading in parallel. Default is 1 to avoid MemoryError")
#General Paths
parse_in.add_argument("--save_path",        default='/media/karsten_dl/QS/Dropbox/Data_Dump/AudioChallenge/savename.csv', help="Path to data")
parse_in.add_argument("--data_path",        default='/media/karsten_dl/QS/Dropbox/Data_Dump/AudioChallenge', help="Path to data")
parse_in.add_argument("--weight_path",      default='/media/karsten_dl/QS/Dropbox/Data_Dump/AudioChallenge/SAVEDATA', help="Where to save training logs.")
#Choice of GPU
parse_in.add_argument("--gpu",              nargs='+', type=int, default=[0],        help="GPU to use for computation")

opt = parse_in.parse_args()


if opt.gpu:
    os.environ["CUDA_DEVICE_ORDER"]   ="PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"]= ','.join(str(x) for x in opt.gpu)



"""======================================="""
"""========= DataLoader =================="""
"""======================================="""
print('\n\n'+'\033[92m'+'Setting Dataloaders...'+'\033[0m'+'\n\n')
torch.manual_seed(opt.seed)
# test_dataloader  = DataLoader(aux.WavDataSet_Internal(load_path=opt.load_path, mode='test', perc_data=opt.perc_data ,seed=opt.seed), batch_size=1, shuffle=True, num_workers=opt.kernels)
test_dataloader  = DataLoader(aux.WavDataSet_External_Test(load_path=opt.load_path, seed=opt.seed), batch_size=1, shuffle=True, num_workers=opt.kernels)



"""==============================================="""
"""=========== Load Network Weights =============="""
"""==============================================="""
hypa = pkl.load(open(opt.weight_path))
network = netlib.RNN_CNN(**hypa.net_params)

loaded_weights = torch.load(opt.weight_path+'/'+netname)
if 'module.' in list(loaded_weights.keys())[0]:
    from collections import OrderedDict
    loaded_weights = OrderedDict((k.split('module.')[-1],v) for k,v in loaded_weights.items())
net.load_state_dict(loaded_weights)



"""======================================="""
"""===== Generate Test Predictions ======="""
"""======================================="""
test_iterator = tqdm(test_dataloader)
performance_collector = {}
for wav_idx, wav_file in enumrate(test_iterator):
    filename  = test_dataloader.dataset.filenames[wav_file['idx']]
    soundfile = Variable(file_dict['sounddata'], volatile=True).transpose(1,2).transpose(0,1).cuda()
    class_prediction = network(soundfile)[-1:,:].data.cpu().numpy()
    performance_collector[filename] = list(class_prediction[np.argsort(class_prediction)[-3:][::-1]])

import csv
### Test dataloader doesn't contain such a dict.
conv_dict = test_dataloader.dataset.conv_to_lab
with open(opt.save_path,'wb') as savefile:
    writer = csv.writer(savefile)
    writer.writerow(['fname','label'])
    for fname in performance_collector.keys():
        writer.writerow[fname,' '.join(conv_dict[x] for x performance_collector[fname])]
