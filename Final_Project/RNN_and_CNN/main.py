# Audio File Classification With Conv-based LSTM RNNs
# @author: Karsten Roth - Heidelberg University

"""======================================="""
"""========= Load Basic Libraries ========"""
"""======================================="""
import os,json,sys,gc,time,datetime,imp,argparse
import torch, torch.nn as nn, torch.optim as to, numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import pickle as pkl

from torch.optim import lr_scheduler
from torch.autograd import Variable
from torch.utils.data import DataLoader

from tqdm import tqdm, trange

import auxiliaries as aux
import Network_Library as netlib
from IPython import embed

import pandas as pd
import ast
import itertools as it

# ###NOTE: (Optional for PyTorch 0.3.1) To avoid the "too many open files"-Error!
# if int(torch.__version__.split(".")[1])>2:
#     torch.multiprocessing.set_sharing_strategy('file_system')
# torch.multiprocessing.set_start_method('spawn')


"""======================================="""
"""========= Input Arguments ============="""
"""======================================="""
parse_in = argparse.ArgumentParser()
#Use textfile for gridsearch
parse_in.add_argument("--gridsearch_file",  type=str, default='', help='Path to gridsearch text file. If not given, standard arguments are used.')
######### Dataloader & Training Arguments ###############
parse_in.add_argument("--n_epochs",         type=int, default=150,      help="Number of training epochs.")
parse_in.add_argument("--bs",               type=int, default=8,        help="Minibatchsize")
parse_in.add_argument("--lr",               type=float, default=1e-3,   help="Choice of learning rate.")
parse_in.add_argument("--optim_method",     default="Adam",             help="Choice of optimizer.")
parse_in.add_argument("--loss",             default="ce",               help="Choice of loss function.")
parse_in.add_argument("--perc_data",        type=float, default=1,     help="Percentage of training data to use.")
parse_in.add_argument("--gamma",            type=float, default=0.1,    help="Reduction of learning rate for lr scheduler.")
parse_in.add_argument("--tau",              type=float, default=65,     help="Epochs after which gamma will reduce the learning rate")
parse_in.add_argument("--kernels",          type=int,   default=8,      help="Number of kernels to use for data loading in parallel. Default is 1 to avoid MemoryError")
parse_in.add_argument("--data_mode",        type=str, default='spec',   help="Mode data should be prepared in.")
parse_in.add_argument("--spatial",          type=int, default=128,      help='Spatial Extend of MFCC')
parse_in.add_argument("--use_weights",      action="store_true",        help="Run testing at the end.")
parse_in.add_argument("--window_size",      type=int, default=40,       help='STFT window size.')
parse_in.add_argument("--overlap",          type=float, default=0.25    help='Overlap percentage between STFT window samples.')
parse_in.add_argument("--use_min",          type=bool, default=True     help='Run normalization on audio data.')

######### NETWORK ARGUMENTS ##################
#RNN Network arguments
parse_in.add_argument("--hidden",           type=int, default=2048,     help='Size of hidden vector in RNN backbone.')
parse_in.add_argument("--lstm_drop",        type=float, default=0,      help='Dropout on LSTM module if multilayered.')
parse_in.add_argument("--lstm_layers",      type=int, default=1,        help='Number of LSTM layers.')
parse_in.add_argument("--lstm_bi",          action='store_true',        help='Whether to run LSTM bidirectional or not.')

parse_in.add_argument("--mode",             type=str,  default='fully-connected', help='Use fully-connected or fully-convolutional feature extraction.')
#CNN Network arguments
parse_in.add_argument("--fcn_layers",       nargs='+', type=int, default=[1024, 224])
parse_in.add_argument("--cnn_filter_set",   nargs='+', type=int, default=[1,128,256])
parse_in.add_argument("--dropout",          type=float,default=0.1)

parse_in.add_argument("--num_classes",      type=int,  default=100)

##################### PATH ARGUMENTS #########################
parse_in.add_argument("--savename",         type=str,   default="",     help="Save Folder Name.")
parse_in.add_argument("--load_path",        default='', help="Path to AudioChallenge Data Folder")
parse_in.add_argument("--save_path",        default='', help="Where to save training logs.")
### Reproduction
parse_in.add_argument("--seed",             type=int, default=1,        help="Reproduction seed")

opt = parse_in.parse_args()




"""============== MAIN FUNCTION =================="""
def main():
    """======================================="""
    """========= DataLoaders ================="""
    """======================================="""
    # print('\n\n'+'\033[92m'+'Setting Dataloaders...'+'\033[0m'+'\n\n')
    torch.manual_seed(opt.seed)

    imp.reload(aux)
    train_dataset    = aux.WavDataSet_Internal_Spec(load_path=opt.load_path, window_size=opt.window_size, use_min=opt.use_min, overlap=opt.overlap, mode='train', perc_data=opt.perc_data, seed=opt.seed)
    train_dataloader = DataLoader(train_dataset, batch_size=opt.bs, shuffle=True, num_workers=opt.kernels, drop_last=True)
    val_dataloader   = DataLoader(aux.WavDataSet_Internal_Spec(load_path=opt.load_path, window_size=opt.window_size, use_min = opt.use_min, overlap=opt.overlap, mode='val', perc_data=opt.perc_data, seed=opt.seed), batch_size=opt.bs, shuffle=False, num_workers=opt.kernels, drop_last=True)
    test_dataloader  = DataLoader(aux.WavDataSet_Internal_Spec(load_path=opt.load_path, window_size=opt.window_size, use_min = opt.use_min, overlap=opt.overlap, mode='test', perc_data=opt.perc_data), batch_size=1, shuffle=False, num_workers=opt.kernels)

    _,input_size = train_dataset[0]['sounddata'].shape


    """======================================="""
    """=========== Load Network =============="""
    """======================================="""
    network_settings = {'hidden' : opt.hidden,
                        'n_classes': opt.num_classes,
                        'bs': opt.bs,
                        'mode': opt.mode,
                        'fcn_setup':  {'setup':[input_size]+opt.fcn_layers},
                        'lstm_setup': {'num_layers':opt.lstm_layers, 'dropout':opt.lstm_drop, 'bidirectional':opt.lstm_bi}
                        }
    opt.network_settings = network_settings


    imp.reload(netlib)
    network = netlib.RNN_SEQ_Net(**network_settings)


    _ = network.cuda()

    f_loss      = nn.CrossEntropyLoss()
    optimizer   = to.Adam(network.parameters(), lr=opt.lr)
    scheduler   = lr_scheduler.StepLR(optimizer, step_size=opt.tau, gamma=opt.gamma)


    """======================================="""
    """=========== Create Logging ============"""
    """======================================="""
    ### Setting Training Savepath
    savename  = network.name if opt.savename=='' else network.name+'_'+opt.savename
    save_path = opt.save_path+'/'+savename

    ### Create Savefolder
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    else:
        count = 1
        while os.path.exists(save_path):
            count       += 1
            save_path    = opt.save_path+"/"+savename+"_"+str(count)
        os.makedirs(save_path)

    ### Saving Setup Parameters
    with open(save_path+'/Setup_Info.txt','w') as f:
        kt = vars(opt)
        json.dump(kt, f)
    pkl.dump(opt,open(save_path+"/hypa.pkl","wb"))


    ### Logging CSV
    logger = {'train loss per epoch': [],
              'train acc per epoch':  [],
              'val loss per epoch':  [],
              'val acc per epoch':  [],
              'time per epoch':  []}

    csv_log_epoch      = aux.CSVlogger(save_path+'/log_epoch.csv',['Epoch', 'Time', 'Training Loss', 'Training Acc', 'Validation Loss', 'Validation Acc'])
    csv_test_res       = aux.CSVlogger(save_path+'/test_results.csv',['Test Loss', 'Test Acc', 'Weights'])

    global best_loss, best_acc
    best_loss, best_acc = np.inf, 0


    """========================================"""
    """====== Plotter Functionality ==========="""
    """========================================"""
    def plotter(x, train_loss, train_acc, val_loss, val_acc, savename='result.svg', title='No title'):
        plt.style.use('ggplot')
        f,ax = plt.subplots(1)
        ax.plot(x, train_loss,'k',label='Training Loss')
        ax.plot(x, val_loss,'--k',label='Validation Loss')
        axt = ax.twinx()
        axt.plot(x, train_acc, 'r', label='Training Acc')
        axt.plot(x, train_loss, '--r', label='Validation Acc')
        ax.set_title(title)
        ax.legend(loc=0)
        axt.legend(loc=2)
        f.suptitle('Metrics')
        f.set_size_inches(15,10)
        f.savefig(savename)
        plt.close()


    """======================================================="""
    """====== Training, Validation & Testing Functions ======="""
    """======================================================="""
    def train(epoch):
        global training_iteration

        training_iterator = tqdm(train_dataloader, position=2)
        training_iterator.set_description('TRAIN: Mean Loss: --- | Mean Acc: ---')

        _ = network.train()


        loss_collection, acc_collection = [], []

        for wav_idx, file_dict in enumerate(training_iterator):
            optimizer.zero_grad()
            network.memory_init_train(make_cuda=True)

            # wav_idx, file_dict = 0,next(iter(training_iterator))
            soundfile = Variable(file_dict['sounddata']).transpose(0,1).cuda()
            soundlabel= Variable(file_dict['label']).cuda()

            class_prediction = network(soundfile)

            #Only take into account last lstm output
            cp_data = class_prediction.data.cpu().numpy()
            acc_val = (np.argmax(cp_data,axis=1)==soundlabel.data.cpu().numpy()).astype(int)

            if opt.use_weights:
                loss = torch.mean(f_loss(class_prediction[-backprop_len:,:], soundlabel)*weights(backprop_len))
            else:
                loss = torch.mean(f_loss(class_prediction, soundlabel))

            #Train
            loss.backward()
            optimizer.step()

            #Save perfomance metrics
            loss_collection.append(loss.data.cpu().numpy()[0])
            acc_collection.append(acc_val)

            if wav_idx%100==0 and wav_idx!=0:
                training_iterator.set_description('TRAIN: Mean Loss: {} | Mean Acc: {}'.format(np.round(np.mean(loss_collection),4), np.round(np.mean(acc_collection),4)))

            if wav_idx==len(train_dataloader)-1:
                logger['train loss per epoch'].append(np.mean(loss_collection))
                logger['train acc per epoch'].append(np.mean(acc_collection))

        torch.cuda.empty_cache()


    def val(epoch):
        global validation_iteration, best_loss, best_acc

        validation_iterator = tqdm(val_dataloader, position=2)
        validation_iterator.set_description('VAL: Mean Loss: --- | Mean Acc: ---')

        _ = network.eval()


        loss_collection = []
        acc_collection  = []


        start_time = time.time()
        for wav_idx, file_dict in enumerate(validation_iterator):
            soundfile = Variable(file_dict['sounddata'],volatile=True).transpose(0,1).cuda()
            soundlabel= Variable(file_dict['label'],volatile=True).cuda()

            network.memory_init_train()
            class_prediction = network(soundfile)


            cp_data = class_prediction.data.cpu().numpy()
            acc_val = np.argmax(cp_data,axis=1)==soundlabel.data.cpu().numpy()

            if opt.use_weights:
                loss = torch.mean(f_loss(class_prediction, soundlabel)*weights)
            else:
                loss = torch.mean(f_loss(class_prediction, soundlabel))

            loss_collection.append(loss.data.cpu().numpy()[0])
            acc_collection.append(acc_val)

            if wav_idx%100==0 and wav_idx!=0:
                validation_iterator.set_description('VAL: Mean Loss: {} | Mean Acc: {}'.format(np.round(np.mean(loss_collection),4), np.round(np.mean(acc_collection),4)))

            if wav_idx==len(val_dataloader)-1:
                logger['val loss per epoch'].append(np.mean(loss_collection))
                logger['val acc per epoch'].append(np.mean(acc_collection))

                if np.mean(acc_collection)>best_acc:
                    torch.save({'epoch': epoch+1,
                                'settings': network_settings,
                                'state dict': network.state_dict(),
                                'optim': optimizer.state_dict},
                                save_path+"/checkpoint.pth.tar")
                    best_acc = np.mean(acc_collection)

        torch.cuda.empty_cache()

    def test():
        ### Run tests on withheld test set.
        test_iterator = tqdm(test_dataloader, position=2)
        test_iterator.set_description('TEST: Mean Loss: --- | Mean Acc: ---')

        _ = network.eval()


        loss_collection = []
        acc_collection  = []


        start_time = time.time()
        for wav_idx, file_dict in enumerate(test_iterator):
            soundfile = Variable(file_dict['sounddata'],volatile=True).transpose(0,1).cuda()
            soundlabel= Variable(file_dict['label'],volatile=True).cuda()

            network.memory_init_test()
            class_prediction = network(soundfile)

            cp_data = class_prediction.data.cpu().numpy()

            acc_val          = np.argmax(cp_data,axis=1)==soundlabel.data.cpu().numpy()

            if opt.use_weights:
                loss = torch.mean(f_loss(class_prediction, soundlabel)*weights(backprop_len))
            else:
                loss = torch.mean(f_loss(class_prediction, soundlabel))

            loss_collection.append(loss.data.cpu().numpy()[0])
            acc_collection.append(acc_val)

            if wav_idx%100==0 and wav_idx!=0:
                test_iterator.set_description('TEST: Mean Loss: {} | Mean Acc: {}'.format(np.round(np.mean(loss_collection),4), np.round(np.mean(acc_collection),4)))

        torch.cuda.empty_cache()
        return np.mean(loss_collection), np.mean(acc_collection)



    """============================="""
    """====== Start Training ======="""
    """============================="""
    # print('\n\n'+'\033[92m'+'Start Training Procedure...'+'\033[0m'+'\n\n')
    epoch_iterator = tqdm(range(opt.n_epochs), position=1, desc='Epoch: ')
    for epoch in epoch_iterator:
        scheduler.step()
        epoch_time = time.time()
        ###### Training ########
        train(epoch)

        ###### Validation #########
        val(epoch)

        ###### Logging Epoch Data ######
        csv_log_epoch.write([epoch, time.time()-epoch_time, logger["train loss per epoch"][-1],
                            logger["train acc per epoch"][-1], logger["val loss per epoch"][-1],
                            logger["val acc per epoch"][-1]])

        ###### Generating Summary Plots #######
        sum_title = 'Max Train Acc: {0:2.5f} | Max Val Acc: {1:2.5f}'.format(np.max(logger['train acc per epoch']),np.max(logger['val acc per epoch']))
        plotter(np.arange(epoch+1),
                logger['train loss per epoch'],
                logger['train acc per epoch'],
                logger['val loss per epoch'],
                logger['val acc per epoch'],
                save_path+'/training_results.svg', sum_title)

        _ = gc.collect()


    print('\n\n'+'\033[92m'+'Testing...'+'\033[0m'+'\n\n')
    test_loss, test_acc = test()
    csv_test_res.write([test_loss, test_acc,aux.gimme_params(network) ])





"""===================== RUN OPTIONAL GRID SEARCH ======================"""
if opt.gridsearch_file != '':
    gridfile = pd.read_table(opt.gridsearch_file, header=None)
    extract = [x.split(':') for x in gridfile[0]]
    extract = [[x[0], ast.literal_eval(x[1])] for x in extract]

    keys = [x[0] for x in extract]
    vals = [x[1] for x in extract]
    vals = list(it.product(*vals))

    d_opt = vars(opt)
    val_it = tqdm(vals, position=0)

    val_it.set_description('GridSearch Iteration')
    for val_set in val_it:
        for i,sub_val in enumerate(val_set):
            d_opt[keys[i]] = sub_val
        main()
else:
    main()
