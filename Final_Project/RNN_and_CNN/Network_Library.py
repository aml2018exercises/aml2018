### PyTorch RNN Network implementation
"""============================="""
"""========= Libraries ========="""
"""============================="""
import os, time, datetime

import torch
import torch.nn as nn
from torch.nn import init
from torchvision import models
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn.modules.module import _addindent

import numpy as np
from collections import OrderedDict




"""===================== NETWORK CLASS ======================"""
class RNN_SEQ_Net(nn.Module):
    def __init__(self, hidden, n_classes, bs, mode='fully', lstm_setup={'num_layers':1, 'dropout':0, 'bidirectional':False}, fcn_setup={'setup':[221,1024,1024,224]}, cnn_setup={'ks':(7,7,7),'setup':(128,512,512)}):
        super(RNN_SEQ_Net, self).__init__()
        self.n_classes = n_classes
        self.hidden = hidden
        self.bs     = bs
        self.mode   = mode
        self.setup  = fcn_setup if mode=='fully' else cnn_setup
        self.lstm_setup = lstm_setup

        self.rnn_backbone = nn.LSTM(self.setup['setup'][-1], self.hidden, **self.lstm_setup)
        self.prep_net     = FullyConnectedNet(**self.setup) if mode=='fully' else CNN1D(**self.setup)
        self.readout      = FullyConnectedNet(setup=[hidden, n_classes])
        self.out           = nn.Softmax(dim=1)

        date = datetime.datetime.now()
        self.name = 'RNN_SEQ_Net-{}_{}_{}_{}_{}_{}'.format(date.year, date.month, date.day, date.hour, date.minute, date.second)

    def memory_init_train(self, make_cuda=True):
        if make_cuda:
            self.rnn_memory   = [Variable(torch.zeros(self.lstm_setup['num_layers'], self.bs, self.hidden)).cuda() for _ in range(2)]
        else:
            self.rnn_memory   = [Variable(torch.zeros(self.lstm_setup['num_layers'], self.bs, self.hidden)) for _ in range(2)]

    def memory_init_test(self, make_cuda=True):
        if make_cuda:
            self.rnn_memory   = [Variable(torch.zeros(self.lstm_setup['num_layers'], 1, self.hidden)).cuda() for _ in range(2)]
        else:
            self.rnn_memory   = [Variable(torch.zeros(self.lstm_setup['num_layers'], 1, self.hidden)) for _ in range(2)]

    def forward(self, x):
        x = self.prep_net(x)
        x,self.rnn_memory = self.rnn_backbone(x, self.rnn_memory)
        x = self.readout(x[-1])
        return self.out(x)


############### 1D Convolutional Feature Extractor Class ####################
class CNN1D(nn.Module):
    def __init__(self, ks, setup):
        super(CNN1D, self).__init__()
        self.ks = ks
        self.setup = [1]+setup

        self.layers = nn.ModuleList([self.sub_layer(ks, f_in, f_out) for ks,f_in,fout in zip(ks, self.setup[:-1], self.setup[1:])])

    def sub_layer(self, ks, f_in, f_out):
        nn.Sequential(nn.Conv1d(f_in, f_out, ks, 1, ks//2), nn.LeakyReLU(0.2))

    def forward(self, x):
        for i,layer in enumerate(self.layers):
             x = layer(x)
             if self.drops is not None and i<len(self.layers)-1:
                 x = self.drops[i](x)
        x = F.avg_pool1d(x)
        return x


############### Fully-Connected Feature Extractor Class ####################
class FullyConnectedNet(nn.Module):
    def __init__(self, setup, dropout=0):
        super(FullyConnectedNet, self).__init__()
        self.setup  = setup
        self.layers = nn.ModuleList([self.sub_layer(n_in, n_out) for n_in, n_out in zip(setup[:-1], setup[1:])])
        self.drops  = nn.ModuleList([nn.Dropout(dropout) for _ in range(len(self.layers)-1)]) if dropout else None

    def sub_layer(self, n_in, n_out):
        return nn.Sequential(nn.Linear(n_in, n_out), nn.LeakyReLU(0.2))

    def forward(self, x):
        for i,layer in enumerate(self.layers):
             x = layer(x)
             if self.drops is not None and i<len(self.layers)-1:
                 x = self.drops[i](x)
        return x
