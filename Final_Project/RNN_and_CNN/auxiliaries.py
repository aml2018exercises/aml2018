### Helper functions for RNN training and data handling
"""=================================="""
"""====== Load Basic Libraries ======"""
"""=================================="""
import numpy as np
import os, sys, time, csv, itertools, copy

from tqdm import tqdm, trange

import torch
import torch.nn as nn
from torch.utils.data import Dataset

import scipy.ndimage.measurements as snm
import skimage.transform as st
from torchvision import transforms
from scipy.io import wavfile

import pandas as pd
from scipy import signal
import librosa

import matplotlib.pyplot as plt


"""
======
Function to convert audio data to STFT spectrogram (log).
======
"""
def conv_2_log_spec(x, sample_rate, window_size, overlap_size, use_min=False):
    window_size   = int(round(window_size * sample_rate / 1e3))
    overlap_size = int(round(overlap_size * sample_rate / 1e3))
    if use_min:
        x = x.astype(np.float32)/(np.max([np.abs(np.min(x)), np.abs(np.max(x))])+1)
    else:
        x = x.astype(np.float32)
    _, _, spec= signal.spectrogram(x, fs=sample_rate, nperseg=window_size, noverlap=overlap_size, detrend=False)
    return np.log(spec.T.astype(np.float32)+1e-10)



#####################################################################################





"""
======
Dataset to load .wav-files. Allows for train and val splits of the original training data set.
Note that there are Datasets for MFCC and Log-Spectrogram Data Preprocessing.
======
"""
class WavDataSet_Internal_MFCC(Dataset):
    def __init__(self, load_path,mode='train',sr_mfcc=44100, n_mfcc=48, perc_data=1, seed=1):

        try:
            data = pd.read_csv(load_path+'/'+mode+'_segment.csv')
        except:
            raise ValueError('Input file {} not available.'.format(load_path+'/'+mode+'_segment.csv'))

        self.load_path    = load_path
        self.filenames    = np.array(data['fname'])
        idxs = list(range(len(self.filenames)))
        np.random.seed(seed)
        np.random.shuffle(idxs)
        idxs = idxs[:int(len(idxs)*perc_data)]

        self.filenames    = self.filenames[idxs]
        self.crosschecked = np.array(data['manually_verified'])[idxs]
        self.class_labels_str = np.array(data['label'])[idxs]

        self.all_avail_class_labels = np.unique(self.class_labels_str)

        self.conv_to_int = {x:i for i,x in enumerate(self.all_avail_class_labels)}
        self.conv_to_lab = {i:x for i,x in enumerate(self.all_avail_class_labels)}

        self.class_labels_int = np.array([self.conv_to_int[x] for x in self.class_labels_str])

        self.n_files = len(self.filenames)

        self.conv_to_mfcc = transforms.Compose([
                                                lambda x: x.astype(np.float32)/np.max(x),
                                                lambda x: librosa.feature.mfcc(x, sr=sr_mfcc, n_mfcc=n_mfcc),
                                                lambda x: torch.Tensor(x)
                                                ])


    def __len__(self):
        return self.n_files

    def __getitem__(self, idx):
        wav_path = self.load_path+"/audio_train_adj/"+self.filenames[idx]
        # wav_path = self.load_path+"/audio_train/"+self.filenames[idx]
        _, data = wavfile.read(wav_path)
        data_mfcc = self.conv_to_mfcc(data)
        label     = self.class_labels_int[idx]

        return {'sounddata':data_mfcc, 'label':label, 'orig':torch.Tensor(data)}

class WavDataSet_Internal_Spec(Dataset):
    def __init__(self, load_path'', mode='train', perc_data=1, max_size=160000, seed=1,
                 use_min=True, window_size=40, overlap=0.25):

        try:
            data = pd.read_csv(load_path+'/'+mode+'_segment.csv')
        except:
            raise ValueError('Input file {} not available.'.format(load_path+'/'+mode+'_segment.csv'))

        self.load_path    = load_path+'/audio_train_adj'
        self.filenames    = np.array(data['fname'])
        self.rng = np.random.RandomState(seed)
        idxs = list(range(len(self.filenames)))
        self.rng.shuffle(idxs)
        idxs = idxs[:int(len(idxs)*perc_data)]


        self.max_size = max_size
        self.filenames        = self.filenames[idxs]
        self.crosschecked     = np.array(data['manually_verified'])[idxs]
        self.class_labels_str = np.array(data['label'])[idxs]

        self.all_avail_class_labels = np.unique(self.class_labels_str)

        self.conv_to_int = {x:i for i,x in enumerate(self.all_avail_class_labels)}
        self.conv_to_lab = {i:x for i,x in enumerate(self.all_avail_class_labels)}

        self.class_labels_int = np.array([self.conv_to_int[x] for x in self.class_labels_str])

        self.n_files = len(self.filenames)
        self.use_min = use_min
        self.window_size = window_size
        self.overlap     = int(window_size*overlap)


    def __len__(self):
        return self.n_files


    def __getitem__(self, idx):
        wav_path = self.load_path+"/"+self.filenames[idx]
        # wav_path = self.load_path+"/audio_train/"+self.filenames[idx]
        sample_rate, data = wavfile.read(wav_path)
        data              = self.size_prep_(data)
        data              = conv_2_log_spec(data, sample_rate, self.window_size, self.overlap, use_min=self.use_min)
        label             = self.class_labels_int[idx]
        return {'sounddata':data, 'label':label}


    def size_prep_(self, x):
        if len(x)>self.max_size:
            rd  = (len(x)-self.max_size)
            idx = self.rng.randint(rd)
            x = x[idx:idx+self.max_size]
            return x
        elif len(x)<self.max_size:
            rd  = (self.max_size-len(x))
            x   = np.pad(x, (rd//2, rd//2+rd%2), mode='constant', constant_values=0)
            return x
        else:
            return x

class WavDataSet_External_Test_MFCC(Dataset):
    def __init__(self, load_path,sr_mfcc=44100, n_mfcc=128):

        try:
            self.filenames = np.array(os.listdir(load_path))
        except:
            raise ValueError('Input folder {} not available.'.format(load_path))

        self.load_path    = load_path

        self.n_files = len(self.filenames)


    def __len__(self):
        return self.n_files

    def __getitem__(self, idx):
        wav_path = self.load_path+"/"+self.filenames[idx]
        _, data = wavfile.read(wav_path)
        data_mfcc = self.conv_to_mfcc(data)
        label     = self.class_labels_int[idx]

        return {'sounddata':data_mfcc, 'label':label, 'orig':torch.Tensor(data), 'idx':idx}

class WavDataSet_External_Test_Spec(Dataset):
    def __init__(self, load_path, perc_data=1., max_size=160000,
                 use_min=True, window_size=40, overlap=0.25):


        self.load_path = load_path + '/audio_test_adj'
        self.max_size  = max_size
        try:
            self.filenames = np.array(os.listdir(self.load_path))
        except:
            raise ValueError('Input folder {} not available.'.format(load_path))

        self.filenames = self.filenames[:int(len(self.filenames)*perc_data)]

        self.n_files = len(self.filenames)
        self.use_min = use_min
        self.window_size = window_size
        self.overlap     = int(window_size*overlap)

    def __len__(self):
        return self.n_files

    def __getitem__(self, idx):
        wav_path = self.load_path+"/"+self.filenames[idx]
        # wav_path = self.load_path+"/audio_train/"+self.filenames[idx]
        sample_rate, data = wavfile.read(wav_path)
        data              = self.t_size_prep_(data)
        data              = conv_2_log_spec(data, sample_rate, self.window_size, self.overlap, use_min=self.use_min)
        return {'sounddata':data}

    def t_size_prep_(self, x):
        if len(x)<self.max_size:
            rd  = (self.max_size-len(x))
            x   = np.pad(x, (rd//2, rd//2+rd%2), mode='constant', constant_values=0)
            return x
        else:
            return x




"""
================
CSV Logging Class
================
"""
class CSVlogger():
    def __init__(self, logname, header_names):
        self.header_names = header_names
        self.logname      = logname
        with open(logname,"a") as csv_file:
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(header_names)
    def write(self, inputs):
        with open(self.logname,"a") as csv_file:
            writer = csv.writer(csv_file, delimiter=",")
            writer.writerow(inputs)



#### Mini Helper to get # of network weights
def gimme_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params
