"""====================== LOAD LIBRARIES ======================="""
import os, pandas as pd, numpy as np, matplotlib.pyplot as plt
from tqdm import tqdm
from scipy.io import wavfile


"""====================== SET REFERENCE PATHS ======================="""
#Where the audio_train and audio_test folders are located
device = '/home/'
medium = ''
base_path           = device+'karsten_dl'+medium+'/Dropbox/Data_Dump/AudioChallenge'
#Where to save the adjusted audio_train & _test-folders
new_data_path_train = device+'karsten_dl'+medium+'/Dropbox/Data_Dump/AudioChallenge/audio_train_adj'
new_data_path_test  = device+'karsten_dl'+medium+'/Dropbox/Data_Dump/AudioChallenge/audio_test_adj'
#Where to save image samples
sample_save_path    = device+'karsten_dl'+medium+'/Dropbox/University/Files/Master/SS18/Advanced_Machine_Learning/aml2018/Final_Project/RNN_and_CNN'


"""====================== CORE FUNCTIONS ============================="""
### Normalization function used in -> return_start_stop()
def normalize(audio):
    audio = audio/max(np.abs(audio))
    return audio

### Suggest lower and upper bound for new non-redundant data
def return_start_stop(audio, resolution=100, window_duration=0.01, amplitude_cutoff=0.001, sample_rate=44100, sensitivity=10, relaxation = 0.1):
    ### Duration of audio file in seconds
    duration    = len(audio) / sample_rate # in samples/sec
    ### Number of iterations to perform sound-checking
    iterations  = int(duration * resolution)
    ### Stepsize
    step = int(sample_rate / resolution)
    window_length = np.floor(sample_rate * window_duration)
    ### Normalized audio power over window length
    audio_power = np.square(normalize(audio)) / window_length
    relaxation_length = int(len(audio)*relaxation)

    start = np.array([])
    stop = np.array([])
    start_acquired = False

    ### Check for localation with sensitivity violations
    for n in range(iterations):
        power = sensitivity * np.sum(audio_power[n * step : int(n * step + window_length)])
        if power > amplitude_cutoff and not start_acquired:
            start = np.append(start, n * step + window_length / 2)
            start_acquired = True
        elif (power <= amplitude_cutoff or n == iterations-1) and start_acquired:
            stop  = np.append(stop, n * step + window_length / 2)
            start_acquired = False

    if start.size == 0:
        start = np.append(start, 0)
        stop = np.append(stop, len(audio))

    start = start.astype(int)
    stop = stop.astype(int)

    lower_bound = int(np.clip(start[0]-relaxation_length,0,None))
    upper_bound = int(np.clip(stop[-1]+relaxation_length,None,len(audio)))

    return lower_bound, upper_bound


"""=================== (optional) SAMPLE GENERATION ======================="""
### Generate Sample Pictures
generate_sample_pictures = False
if generate_sample_pictures:
    print('Generating Samples...')
    for jk in tqdm(range(3)):
        sample_sound_paths = np.random.choice(os.listdir(base_path+"/audio_train"),16)
        sample_sounds_loaded = []

        for sample_sound_path in sample_sound_paths:
            sample_sounds_loaded.append(wavfile.read(base_path+"/audio_train/"+sample_sound_path))

        intervals = []
        for sample_rate, audio in sample_sounds_loaded:
            intervals.append(return_start_stop(audio, resolution=100, window_duration=0.01, amplitude_cutoff=0.001, sample_rate=sample_rate, sensitivity=10, relaxation=0.1))

        f,axs = plt.subplots(len(sample_sounds_loaded)*2//4,4)
        axs = axs.reshape(-1)
        for i in range(len(sample_sounds_loaded)):
            axs[i*2].plot(sample_sounds_loaded[i][-1])
            axs[i*2].set_title('Original')
            axs[i*2+1].plot(sample_sounds_loaded[i][-1][intervals[i][0]:intervals[i][1]])
            axs[i*2+1].set_title('Reduced')
            axs[i*2+1].set_yticks([])
        f.set_size_inches(25,20)
        plt.subplots_adjust(top = 0.9)
        f.tight_layout()
        f.savefig(sample_save_path+'/reduction_'+str(jk+1)+'.png')
        plt.close()



"""=========================== EXAMINE DATASET =================="""
### Minimal Data Exploration Functions for mean/median audio file length
print('\033[92m'+'Generating adjusted training files...'+'\033[0m')
all_files = os.listdir(base_path+"/audio_train/")
all_files = os.listdir(new_data_path_train)
lens = []
for filename in tqdm(all_files):
    sample_rate, audio = wavfile.read(new_data_path_train+'/'+filename)
    lens.append(len(audio)/sample_rate)
for filename in tqdm(os.listdir(new_data_path_test)):
    sample_rate, audio = wavfile.read(new_data_path_test+'/'+filename)
    lens.append(len(audio)/sample_rate)
print('Min. Len: {}, Max. Len: {}'.format(np.min(lens), np.max(lens)))
print('Mean. Len: {}, Median. Len: {}'.format(np.mean(np.array(lens)*44100), np.median(np.array(lens)*44100)))




"""=========================== GENERATE NEW DATASETS ========================"""
### Generate new wavfiles for cropped audio files
if not os.path.exists(new_data_path_train):
    os.makedirs(new_data_path_train)
for n,filename in enumerate(tqdm(all_files)):
    sample_rate, interm_audio = wavfile.read(base_path+"/audio_train/"+filename)
    lower_bound, upper_bound = return_start_stop(interm_audio, resolution=100, window_duration=0.01, amplitude_cutoff=0.001, sample_rate=sample_rate, sensitivity=10, relaxation=0.1)

    wavfile.write(new_data_path_train+'/'+filename, sample_rate, interm_audio[lower_bound:upper_bound])


### Updating test files
print('\033[92m'+'Generating adjusted test files...'+'\033[0m')
all_files = os.listdir(base_path+"/audio_test/")
if not os.path.exists(new_data_path_test):
    os.makedirs(new_data_path_test)
for n,filename in enumerate(tqdm(all_files)):
    sample_rate, interm_audio = wavfile.read(base_path+"/audio_test/"+filename)
    lower_bound, upper_bound = return_start_stop(interm_audio, resolution=100, window_duration=0.01, amplitude_cutoff=0.001, sample_rate=sample_rate, sensitivity=10, relaxation=0.1)

    wavfile.write(new_data_path_test+'/'+filename, sample_rate, interm_audio[lower_bound:upper_bound])
