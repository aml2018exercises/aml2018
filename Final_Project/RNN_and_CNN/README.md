## Usage Documentation to train the RNN audiotagging network

To train and setup the audio sequence tagging pipeline, two things are needed:

* A Text File placed into the `GridSearch`-folder if a gridsearch of any sort is to be performed. If not, this part can be ignored and all relevant parameters are to be passed directly to the `main.py`-file. The gridsearchfile setup should be self-explanatory by looking at example files placed into that folder.
* Running the main training file `main.py`. Besides network parameters, paths to a save folder for logging files and intermediate checkpoints and the audio data need to be given. Please look at the file - each input argument is explained respectively.