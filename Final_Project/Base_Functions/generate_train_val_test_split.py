import os, numpy as np, pandas as pd

csv_name_path = '/home/karsten_dl/Dropbox/Data_Dump/AudioChallenge'
csv_data      = pd.read_csv(csv_name_path+'/train_segment.csv', header=0)
csv_data
np.unique(np.array(csv_data['label']))
train, val, test = 0.7,0.2,0.1

rng = np.random.RandomState(1)
idx_list = list(range(len(csv_data)))
rng.shuffle(idx_list)

train_idxs = idx_list[:int(len(idx_list)*train)]
val_idxs   = idx_list[int(len(idx_list)*train):int(len(idx_list)*(train+val))]
test_idxs  = idx_list[int(len(idx_list)*(train+val)):]

train_data = csv_data.loc[train_idxs]
val_data   = csv_data.loc[val_idxs]
test_data  = csv_data.loc[test_idxs]

train_data.to_csv(csv_name_path+'/train_segment.csv')
val_data.to_csv(csv_name_path+'/val_segment.csv')
test_data.to_csv(csv_name_path+'/test_segment.csv')
